package com.myzee;

import java.util.HashMap;
import java.util.IdentityHashMap;

public class TestHashMapandIndentityHashMap {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Integer i = new Integer(10);
		Integer j = new Integer(10);
		System.out.println("Normal HashMap");
		
		/*
		 * HashMap uses .equals method to compare two keys
		 * 
		 */
		HashMap<Integer, String> hm = new HashMap<>();
		hm.put(i, "ten");
		hm.put(j, "againten");		//it overwrites the 'ten' value
		System.out.println(hm);
		
		/*
		 * IdentityHashMap uses == operator to compare two keys
		 */
		
		System.out.println("\nusing IdentityHashMap");	
		IdentityHashMap<Integer, String> ihm = new IdentityHashMap<>();
		ihm.put(i, "ten");
		ihm.put(j, "tenagain");
		System.out.println(ihm);		// it stores both because, i and j are different references to different objects.
	}

}
